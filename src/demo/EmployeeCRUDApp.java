package demo;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import entity.Employee;

public class EmployeeCRUDApp {

	public static void main(String[] args) {
		
		SessionFactory factory = new Configuration()
									.configure("hibernate.cfg.xml")
									.addAnnotatedClass(Employee.class)
									.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try {
			Employee employee = new Employee("Enrique", "Vallejo", "ING");
			
			session.beginTransaction();
			
			System.out.println("Saving Employee");
			session.save(employee);
			session.getTransaction().commit();
			System.out.println("Employee saved");
			
			session = factory.getCurrentSession();
			session.beginTransaction();
			System.out.println("Retrieving employee");
			session.get(Employee.class, employee.getId());
			System.out.println("Employee " + employee);
			session.getTransaction().commit();
			
			session = factory.getCurrentSession();
			session.beginTransaction();
		    System.out.println("Updating Company");
		    List<Employee> employees = session.createQuery("from Employee s where s.company ='Babel'", Employee.class).getResultList();
		    // employee.setCompany("Babel");
		    
		    employees.forEach(employ -> {
		    	System.out.println(employ.toString());
		    });
		    
		    session.getTransaction().commit();
		    System.out.println("Company updated");
			
		} finally {
			session.close();
		}

	}

}
